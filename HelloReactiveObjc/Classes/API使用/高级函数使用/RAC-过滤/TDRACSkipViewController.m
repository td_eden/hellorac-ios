//
//  TDRACSkipViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/11/26.
//

#import "TDRACSkipViewController.h"

@interface TDRACSkipViewController ()

@end

@implementation TDRACSkipViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self skipDemo];
}

/// 跳跃，如下skip传入2 跳过前面两个值
- (void)skipDemo {
    // 实际用处: 比如 后台返回的数据前面几个没用，我们想跳跃过去，便可以用skip
    RACSubject *subject = [RACSubject subject];
    [[subject skip:2] subscribeNext:^(id  _Nullable x) {
        NSLog(@"跳跃 - %@", x);
    }];
    [subject sendNext:@1];
    [subject sendNext:@2];
    [subject sendNext:@3];
}

@end
