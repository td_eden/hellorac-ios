//
//  TDRACIgnoreViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/11/26.
//

#import "TDRACIgnoreViewController.h"

@interface TDRACIgnoreViewController ()

@end

@implementation TDRACIgnoreViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self ignoreDemo];
}

/// ignore: 忽略一些值
- (void)ignoreDemo {
    // ignoreValues: 标识忽略所有的值   [subject ignoreValues];
    RACSubject *subject = [RACSubject subject];
    RACSignal *signal = [subject ignore:@2];
    [signal subscribeNext:^(id  _Nullable x) {
        NSLog(@"ignore - %@", x);
    }];
    [subject sendNext:@2];
}

@end
