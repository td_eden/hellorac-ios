//
//  TDRACDistinctUntilChangedViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/11/26.
//

#import "TDRACDistinctUntilChangedViewController.h"

@interface TDRACDistinctUntilChangedViewController ()

@end

@implementation TDRACDistinctUntilChangedViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self distinctUntilChangedDemo];
}

/// distinctUntilChanged: 如果当前的值跟上一次的值一样，就不会被订阅到
- (void)distinctUntilChangedDemo {
    RACSubject *subject = [RACSubject subject];
    [[subject distinctUntilChanged] subscribeNext:^(id  _Nullable x) {
        NSLog(@"distinctUntilChanged - %@", x);
    }];
    [subject sendNext:@1];
    [subject sendNext:@2];
    [subject sendNext:@2];  // 不会被订阅
}

@end
