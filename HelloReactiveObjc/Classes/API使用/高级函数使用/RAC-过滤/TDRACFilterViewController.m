//
//  TDRACFilterViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//

#import "TDRACFilterViewController.h"

@interface TDRACFilterViewController ()

@property (nonatomic, strong) UITextField *textField;

@end

@implementation TDRACFilterViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = TDRACFilter;
    
    CGRect textRect = CGRectMake((UIScreen.width-200)/2, (UIScreen.height-40)/2-200, 200, 40);
    _textField = [[UITextField alloc] initWithFrame:textRect];
    _textField.borderStyle = UITextBorderStyleRoundedRect;
    [self.view addSubview:_textField];
    // filter一般和文本框一起用
    [self textFieldDemo];
}

/// 一般和文本框一起用，添加过滤条件
- (void)textFieldDemo {
    // 只有当文本框的内容长度大于5，才获取文本框里的内容
    [[self.textField.rac_textSignal filter:^BOOL(NSString * _Nullable value) {
        // value 源信号的内容
        return [value length] > 5;
        // 返回值 就是过滤条件。只有满足这个条件才能获取到内容
    }] subscribeNext:^(NSString * _Nullable x) {
        NSLog(@"一般和文本框一起用 - %@", x);
    }];
}

@end
