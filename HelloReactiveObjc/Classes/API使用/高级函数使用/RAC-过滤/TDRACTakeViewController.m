//
//  TDRACTakeViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/11/26.
//

#import "TDRACTakeViewController.h"

@interface TDRACTakeViewController ()

@end

@implementation TDRACTakeViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 过滤方法有好多种，如下代码，从不同情况下进行了分析
    [self showAlert];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self showAlert];
}

- (void)showAlert {
    [self showAlertControllerWithTitle:@"take" message:nil actionTitles:@[ @"take", @"takeLast", @"takeUntil"] cancelTitle:@"取消/查看控制台" preferredStyle:UIAlertControllerStyleAlert callBack:^(NSInteger index) {
        switch (index) {
            case 0:
                [self takeDemo];                // take
                break;
            case 1:
                [self takeLastDemo];            // takeLast
                break;
            case 2:
                [self takeUntilDemo];           // takeUntil
                break;
        }
    }];
}

/// take: 可以屏蔽一些值，去掉前面几个值。这里take为2，则只拿到前两个值
- (void)takeDemo {
    RACSubject *subject = [RACSubject subject];
    [[subject take:2] subscribeNext:^(id  _Nullable x) {
        NSLog(@"take - %@", x);
    }];
    [subject sendNext:@1];
    [subject sendNext:@2];
    [subject sendNext:@3];
}

/// takeLast: 和take用法一样，不过它取的是最后几个值，如下，则取最后2个值
- (void)takeLastDemo {
    /// 注意点: takeLast一定要调用sendCompleted，告诉它发送完成了，这样才能取到最后几个值
    RACSubject *subject = [RACSubject subject];
    [[subject takeLast:2] subscribeNext:^(id  _Nullable x) {
        NSLog(@"takeLast - %@", x);
    }];
    [subject sendNext:@1];
    [subject sendNext:@2];
    [subject sendNext:@3];
    [subject sendCompleted];
}

/// takeUntil: 给takeUntil传的是哪个信号，那么当这个信号发送信号或sendCompleted，就不能再接受源信号的内容了
- (void)takeUntilDemo {
    RACSubject *subject1 = [RACSubject subject];
    RACSubject *subject2 = [RACSubject subject];
    [[subject1 takeUntil:subject2] subscribeNext:^(id  _Nullable x) {
        NSLog(@"takeUntil - %@", x);
    }];
    [subject1 sendNext:@1];
    [subject1 sendNext:@2];
    [subject2 sendNext:@3];         // 1
    // [subject2 sendCompleted];    // 或2
    [subject1 sendNext:@4];
}

@end
