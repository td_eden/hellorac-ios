//
//  TDRACMapViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//  我们想要拦截服务器返回的数据，给数据拼接特定的东西或想对数据进行操作从而更改返回值可使用RAC-映射

#import "TDRACMapViewController.h"

@interface TDRACMapViewController ()

@property (nonatomic, strong) UITextField *textField;

@end

@implementation TDRACMapViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect textRect = CGRectMake((UIScreen.width-200)/2, (UIScreen.height-40)/2-200, 200, 40);
    _textField = [[UITextField alloc] initWithFrame:textRect];
    _textField.borderStyle = UITextBorderStyleRoundedRect;
    [self.view addSubview:_textField];
    //[_textField becomeFirstResponder];
    
    /**
     RAC的映射在实际开发中有什么用呢？比如我们想要拦截服务器返回的数据，给数据拼接特定的东西或想对数据进行操作从而更改返回值，类似于这样的情况下，我们便可以考虑用RAC的映射
     */
    [self racMap];
}

/// map 映射
- (void)racMap {
    // flattenMap 的底层实现是通过bind实现的
    // map 的底层实现是通过 flattenMap 实现的
    
    // map 事例
    [[_textField.rac_textSignal map:^id _Nullable(NSString * _Nullable value) {
        return [NSString stringWithFormat:@"%@🍺🍺🍺🍺🍺🍺🍺",value];
    }] subscribeNext:^(id  _Nullable x) {
        NSLog(@"-->%@", x);
    }];
}

@end
