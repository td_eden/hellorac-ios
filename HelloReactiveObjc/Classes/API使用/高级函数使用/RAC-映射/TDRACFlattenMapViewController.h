//
//  TDRACFlattenMapViewController.h
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/11/26.
//  我们想要拦截服务器返回的数据，给数据拼接特定的东西或想对数据进行操作从而更改返回值可使用RAC-映射

#import "TDRACBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TDRACFlattenMapViewController : TDRACBaseViewController

@end

NS_ASSUME_NONNULL_END
