//
//  TDRACCombineLatestViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//  把多个信号聚合成你想要的信号。使用场景: 比如-当多个输入框都有值的时候按钮才可点击。

#import "TDRACCombineLatestWithViewController.h"

@interface TDRACCombineLatestViewController ()

@property (nonatomic, strong) RACSignal *signal1;
@property (nonatomic, strong) RACSignal *signal2;

@end

@implementation TDRACCombineLatestViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 首先创建两个信号signal1和signal2来演示
    _signal1 = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [subscriber sendNext:@"signal1-->🍺🍺🍺🍺🍺🍺🍺"];
        [subscriber sendCompleted];
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"signal1销毁了");
        }];
    }];
    _signal2 = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [subscriber sendNext:@"signal2-->🍺🍺🍺🍺🍺🍺🍺"];
        [subscriber sendCompleted];
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"signal2销毁了");
        }];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self racCombineLatestWith];
    
    [self showAlertControllerWithTitle:nil message:@"将多个信号合并起来，并且拿到各个信号的最新的值,必须每个合并的signal至少都有过一次sendNext，才会触发合并的信号(订阅者每次接收的参数都是所有信号的最新值),不论触发哪个信号都会触发合并的信号" actionTitles:nil cancelTitle:@"查看控制台了解" preferredStyle:UIAlertControllerStyleAlert callBack:nil];
}

/// combineLatestWith -- 将多个信号合并起来，并且拿到各个信号的最新的值,必须每个合并的signal至少都有过一次sendNext，才会触发合并的信号(订阅者每次接收的参数都是所有信号的最新值),不论触发哪个信号都会触发合并的信号
- (void)racCombineLatestWith {
    // 一个信号signal3去监听signal1和signal2，每次回调两个信号的最新值
    RACSignal *signal3 = [_signal1 combineLatestWith:_signal2];
    [signal3 subscribeNext:^(id  _Nullable x) {
        NSLog(@"%@", x);
    }];
}

@end
