//
//  TDRACThenViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//  使用需求: 有两部分数据，想让上部分先进行网络请求但是过滤掉数据，然后进行下部分的，拿到下部分数据

#import "TDRACThenViewController.h"

@interface TDRACThenViewController ()

@property (nonatomic, strong) RACSignal *signal1;
@property (nonatomic, strong) RACSignal *signal2;

@end

@implementation TDRACThenViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 首先创建两个信号signal1和signal2来演示
    _signal1 = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [subscriber sendNext:@"signal1-->🍺🍺🍺🍺🍺🍺🍺"];
        [subscriber sendCompleted];
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"signal1销毁了");
        }];
    }];
    _signal2 = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [subscriber sendNext:@"signal2-->🍺🍺🍺🍺🍺🍺🍺"];
        [subscriber sendCompleted];
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"signal2销毁了");
        }];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self racThen];
    
    [self showAlertControllerWithTitle:nil message:@"用于连接两个信号，等待第一个信号完成，才会连接then返回的信号" actionTitles:nil cancelTitle:@"查看控制台了解" preferredStyle:UIAlertControllerStyleAlert callBack:nil];
}

/// then -- 用于连接两个信号，等待第一个信号完成，才会连接then返回的信号
- (void)racThen {
    @weakify(self)
    RACSignal *signal3 = [_signal1 then:^RACSignal * _Nonnull{
        @strongify(self)
        return self.signal2;
    }];
    [signal3 subscribeNext:^(id  _Nullable x) {
        NSLog(@"signal3-->%@", x);
    }];
}

@end
