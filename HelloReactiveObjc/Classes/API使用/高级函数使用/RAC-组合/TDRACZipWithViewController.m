//
//  TDRACZipWithViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//  zipWith:把两个信号压缩成一个信号，只有当两个信号同时发出信号内容时，并且把两个信号的内容合并成一个元祖，才会触发压缩流的next事件

#import "TDRACZipWithViewController.h"

@interface TDRACZipWithViewController ()

@property (nonatomic, strong) RACSignal *signal1;
@property (nonatomic, strong) RACSignal *signal2;

@end

@implementation TDRACZipWithViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 首先创建两个信号signal1和signal2来演示
    _signal1 = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [subscriber sendNext:@"signal1-->🍺🍺🍺🍺🍺🍺🍺"];
        [subscriber sendCompleted];
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"signal1销毁了");
        }];
    }];
    _signal2 = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [subscriber sendNext:@"signal2-->🍺🍺🍺🍺🍺🍺🍺"];
        [subscriber sendCompleted];
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"signal2销毁了");
        }];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    // **-zipWith-**: 当一个界面多个请求的时候，要等所有请求完成才更新UI
    [self racZipWith];
    
    [self showAlertControllerWithTitle:nil message:@"把两个信号压缩成一个信号，只有当两个信号都发出信号内容时，才会触发" actionTitles:nil cancelTitle:@"查看控制台了解" preferredStyle:UIAlertControllerStyleAlert callBack:nil];
}

/// zipWith -- 把两个信号压缩成一个信号，只有当两个信号都发出信号内容时，才会触发
- (void)racZipWith {
    RACSignal *signal3 = [_signal1 zipWith:_signal2];
    [signal3 subscribeNext:^(id  _Nullable x) {
        NSLog(@"signal3-->%@",x);
    }];
}

@end
