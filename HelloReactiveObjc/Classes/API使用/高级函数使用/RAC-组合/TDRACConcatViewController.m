//
//  TDRACConcatViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//  使用需求：有两部分数据：想让上部分先执行，完了之后再让下部分执行（都可获取值）

#import "TDRACConcatViewController.h"

@interface TDRACConcatViewController ()

@property (nonatomic, strong) RACSignal *signal1;
@property (nonatomic, strong) RACSignal *signal2;

@end

@implementation TDRACConcatViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _signal1 = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [subscriber sendNext:@"afn请求后 上 部分数据"];
        [subscriber sendCompleted]; // 必须要调用sendCompleted方法！
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"signal1销毁了");
        }];
    }];
    _signal2 = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [subscriber sendNext:@"afn请求后 下 部分数据"];
        [subscriber sendCompleted];
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"signal2销毁了");
        }];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self racConcat];
    
    [self showAlertControllerWithTitle:nil message:@"当多个信号发出的时候，有顺序的接收信号" actionTitles:nil cancelTitle:@"查看控制台了解" preferredStyle:UIAlertControllerStyleAlert callBack:nil];
}

//**-注意-**：concat，第一个信号必须要调用sendCompleted
/// concat -- 按顺序去链接
- (void)racConcat {
    // 创建组合信号
    RACSignal *signal3 = [_signal1 concat:_signal2];
    // 订阅组合信号
    [signal3 subscribeNext:^(id  _Nullable x) {
        NSLog(@"signal3-->%@", x);
    }];
}

@end
