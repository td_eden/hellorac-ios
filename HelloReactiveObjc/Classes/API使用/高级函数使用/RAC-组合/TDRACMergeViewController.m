//
//  TDRACMergeViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//

#import "TDRACMergeViewController.h"

@interface TDRACMergeViewController ()

@property (nonatomic, strong) RACSignal *signal1;
@property (nonatomic, strong) RACSignal *signal2;

@end

@implementation TDRACMergeViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 首先创建两个信号signal1和signal2来演示
    _signal1 = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [subscriber sendNext:@"signal1-->🍺🍺🍺🍺🍺🍺🍺"];
        [subscriber sendCompleted];
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"signal1销毁了");
        }];
    }];
    _signal2 = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [subscriber sendNext:@"signal2-->🍺🍺🍺🍺🍺🍺🍺"];
        [subscriber sendCompleted];
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"signal2销毁了");
        }];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self racMerge];
    
    [self showAlertControllerWithTitle:nil message:@"把多个信号合并为一个信号来监听，任何一个信号有新值的时候就会调用" actionTitles:nil cancelTitle:@"查看控制台了解" preferredStyle:UIAlertControllerStyleAlert callBack:nil];
}

/// merge -- 把多个信号合并为一个信号来监听，任何一个信号有新值的时候就会调用
- (void)racMerge {
    // 一个信号signal3去监听signal1和signal2，每次回调一个信号
    RACSignal *signal3 = [_signal1 merge:_signal2];
    [signal3 subscribeNext:^(id  _Nullable x) {
        NSLog(@"signal3-->%@", x);
    }];
}

@end
