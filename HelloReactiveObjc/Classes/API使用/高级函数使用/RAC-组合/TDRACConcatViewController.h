//
//  TDRACConcatViewController.h
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//  使用需求：有两部分数据：想让上部分先执行，完了之后再让下部分执行（都可获取值）

#import "TDRACBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TDRACConcatViewController : TDRACBaseViewController

@end

NS_ASSUME_NONNULL_END
