//
//  TDRACReduceViewController.h
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//  把多个信号聚合成你想要的信号。使用场景: 比如-当多个输入框都有值的时候按钮才可点击。

#import "TDRACBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TDRACReduceViewController : TDRACBaseViewController

@end

NS_ASSUME_NONNULL_END
