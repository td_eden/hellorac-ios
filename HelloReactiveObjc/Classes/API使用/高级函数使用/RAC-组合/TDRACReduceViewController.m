//
//  TDRACReduceViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//  把多个信号聚合成你想要的信号。使用场景: 比如-当多个输入框都有值的时候按钮才可点击。

#import "TDRACReduceViewController.h"

@interface TDRACReduceViewController ()

@property (nonatomic, strong) RACSignal *signal1;
@property (nonatomic, strong) RACSignal *signal2;

@end

@implementation TDRACReduceViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 首先创建两个信号signal1和signal2来演示
    _signal1 = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [subscriber sendNext:@"signal1-->🍺🍺🍺🍺🍺🍺🍺"];
        [subscriber sendCompleted];
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"signal1销毁了");
        }];
    }];
    _signal2 = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [subscriber sendNext:@"signal2-->🍺🍺🍺🍺🍺🍺🍺"];
        [subscriber sendCompleted];
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"signal2销毁了");
        }];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self racReduce];
    
    [self showAlertControllerWithTitle:nil message:@"聚合 -- 把多个信号的值按照自定义的组合返回" actionTitles:nil cancelTitle:@"查看控制台了解" preferredStyle:UIAlertControllerStyleAlert callBack:nil];
}

/// reduce 聚合 -- 把多个信号的值按照自定义的组合返回
- (void)racReduce {
    RACSignal *signal3 = [RACSignal combineLatest:@[_signal1, _signal2] reduce:^id(NSString *s1, NSString *s2){
        return [NSString stringWithFormat:@"%@ %@", s1, s2];
    }];
    [signal3 subscribeNext:^(id  _Nullable x) {
        NSLog(@"%@", x);
    }];
    
    
    
    
    /* 如下示例: 
    
    // 思路--- 就是把输入框输入值的信号都聚合成按钮是否能点击的信号。
    RACSignal *combinSignal = [RACSignal combineLatest:@[self.accountField.rac_textSignal, self.pwdField.rac_textSignal] reduce:^id(NSString *account, NSString *pwd){
        // reduce里的参数一定要和combineLatest数组里的一一对应。
        // block: 只要源信号发送内容，就会调用，组合成一个新值。
        NSLog(@"%@ %@", account, pwd);
        return @(account.length && pwd.length);
    }];

    RAC(self.loginBtn, enabled) = combinSignal;
     
    */
}


@end
