//
//  TDRACFuncListViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/11/26.
//

#import "TDRACFuncListViewController.h"

@interface TDRACFuncListViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataSource;

@end

@implementation TDRACFuncListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:({
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorInset = UIEdgeInsetsMake(0, 12, 0, -12);
        _tableView;
    })];
}

- (void)setTitle:(NSString *)title {
    [super setTitle:title];
    if ([title isEqualToString:TDRACCombination]) {
        _dataSource = @[TDRACConcat, TDRACThen, TDRACMerge, TDRACZipWith, TDRACCombineLatest, TDRACReduce];
    } else if ([title isEqualToString:TDRACMapping]) {
        _dataSource = @[TDRACMap, TDRACFlattenMap];
    } else if ([title isEqualToString:TDRACFiltration]) {
        _dataSource = @[TDRACFilter, TDRACIgnore, TDRACDistinctUntilChanged, TDRACTake, TDRACSkip];
    }
    [self.tableView reloadData];
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *text = self.dataSource[indexPath.row];
    // 调整首字母大写
    NSString *name = [text stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[text substringToIndex:1] capitalizedString]];
    NSString *className = [NSString stringWithFormat:@"TDRAC%@ViewController", name];
    UIViewController *vc = [[NSClassFromString(className) alloc] init];
    vc.title = className;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource == nil ? 0 : self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = self.dataSource[indexPath.row];
    return cell;
}

@end
