//
//  TDRACMulticastConnectionViewController.h
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//  RACMulticastConnection: 当有多个订阅者，但是我们只想发送一个信号的时候可以使用

#import "TDRACBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TDRACMulticastConnectionViewController : TDRACBaseViewController

@end

NS_ASSUME_NONNULL_END
