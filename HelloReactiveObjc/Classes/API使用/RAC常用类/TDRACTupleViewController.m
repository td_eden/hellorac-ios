//
//  TDRACTupleViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//

#import "TDRACTupleViewController.h"

@interface TDRACTupleViewController ()

@end

@implementation TDRACTupleViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = TDRACTuple;
    
    [self racTuple];
}

/// RACTuple（元组）-- 其内部就是封装了数组，用起来跟数组差不多
- (void)racTuple {
    // 通过定值创建RACTuple
    RACTuple *tuple1 = [RACTuple tupleWithObjects:@"1", @"2", @"3", nil];
    // 利用 RAC 宏快速封装
    RACTuple *tuple2 = RACTuplePack(@"1", @"2", @"3");
    // 从别的数组中获取内容
    RACTuple *tuple3 = [RACTuple tupleWithObjectsFromArray:@[@"1", @"2", @"3"]];
    
    NSLog(@"元祖-->%@", tuple1[0]);
    NSLog(@"第一个元素-->%@", [tuple2 first]);
    NSLog(@"最后一个元素-->%@", [tuple3 last]);
}



- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self demoSequence];
}

/// 演示 快速高效的遍历数组和字典
- (void)demoSequence {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"flags.plist" ofType:nil];
    NSArray *dictArray = [NSArray arrayWithContentsOfFile:path];
    
    [dictArray.rac_sequence.signal subscribeNext:^(NSDictionary *  _Nullable x) {
        NSLog(@"%@", x);
        
        // RACTupleUnpack宏: 专门用来解析元组 元组里有几个值，宏的参数就必须填几个
        // RACTupleUnpack(NSString *key, NSString *value) = x;
    } error:^(NSError * _Nullable error) {
        NSLog(@"===error===");
    } completed:^{
        NSLog(@"===completed===");
    }];
}

@end

/**
 RACTuple: 元组类,类似NSArray,用来包装值
 RACTuplePack: 把数据包装成RACTuple（元组类）
 RACTupleUnpack: 把RACTuple（元组类）解包成对应的数据
 */
