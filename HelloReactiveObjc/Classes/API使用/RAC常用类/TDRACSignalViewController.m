//
//  TDRACSignalViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//  信号类

#import "TDRACSignalViewController.h"

@interface TDRACSignalViewController ()

@end

@implementation TDRACSignalViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = TDRACSignal;
    
    [self racSignal];
}

/// RACSignal
- (void)racSignal {
    // 1.创建信号
    RACSignal *signal = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        
        // 3.发送信号
        [subscriber sendNext:@"发送信号🍺🍺"];
        // [subscriber sendCompleted];
        // [subscriber sendError:nil];
        
        // 4.取消信号，如果信号想要被取消，就必须返回一个RACDisposable
        // Q:信号什么时候被取消: 1.自动取消，当一个信号的订阅者被销毁时会自动取消订阅; 2.手动取消;
        // Q:block什么时候被调用: 一旦一个信号被取消订阅就会调用
        // Q:block作用: 当信号被取消时用于清空一些资源
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"销毁了🍺🍺🍺");
        }];
    }];
    
    // 2.订阅信号 subscribeNext （每次订阅都会触发RACDisposable）
    [signal subscribeNext:^(id  _Nullable x) {
        NSLog(@"订阅信号🍺 -> %@", x); // 每调用一次订阅就会触发创建时候传入的block
    } error:^(NSError * _Nullable error) {
        NSLog(@"sendError🍺 -> %@", error);
    } completed:^{
        NSLog(@"sendCompleted🍺");
    }];
    
    
    // 取消订阅 (只要订阅信号就会返回一个取消订阅信号的类)
    // [disposable dispose];
}

@end

/**
 RACSignal底层实现:

 1、创建信号，内部会创建一个RACDynamicSignal,首先把didSubscribe保存到信号中，还不会触发。

 2、当信号被订阅，也就是调用signal的subscribeNext:nextBlock
    2.1、subscribeNext内部会创建订阅者subscriber，并且把nextBlock保存到subscriber中。
    2.2、subscribeNext内部会调用siganl的didSubscribe

 3、siganl的didSubscribe中调用[subscriber sendNext:@"111"];
    3.1、sendNext底层其实就是执行subscriber的nextBlock
 */


/**
 RACSignal总结:
 一.核心：
    1.核心：信号类
    2.信号类的作用：只要有数据改变就会把数据包装成信号传递出去
    3.只要有数据改变就会有信号发出
    4.数据发出，并不是信号类发出，信号类不能发送数据
 二.使用方法：
    1.创建信号
    2.订阅信号
 三.实现思路：
    1.当一个信号被订阅，创建订阅者，并把nextBlock保存到订阅者里面。
    2.创建的时候会返回 [RACDynamicSignal createSignal:didSubscribe];
    3.调用RACDynamicSignal的didSubscribe
    4.发送信号[subscriber sendNext:value];
    5.拿到订阅者的nextBlock调用
 */
