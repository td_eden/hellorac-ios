//
//  TDRACCommandViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//  RACCommand:RAC中用于处理事件的类，可以把事件如何处理,事件中的数据如何传递，包装到这个类中，他可以很方便的监控事件的执行过程

#import "TDRACCommandViewController.h"

@interface TDRACCommandViewController ()

@end

@implementation TDRACCommandViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = TDRACCommand;
    [self showAlert];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self showAlert];
}

- (void)showAlert {
    [self showAlertControllerWithTitle:TDRACCommand message:nil actionTitles:@[@"普通做法", @"一般做法", @"高级做法", @"switchToLatest", @"监听信号状态"] cancelTitle:@"取消/查看控制台" preferredStyle:UIAlertControllerStyleAlert callBack:^(NSInteger index) {
        switch (index) {
            case 0:
                [self putongCommand];   // 普通做法
                break;
            case 1:
                [self yibanCommand];    // 一般做法
                break;
            case 2:
                [self gaojiCommand];    // 高级做法
                break;
            case 3:
                [self switchToLatest];  // switchToLatest 用于信号中信号 示例
                break;
            case 4:
                [self racCommand];      //  可以监听信号的状态等
                break;
        }
    }];
}

- (void)putongCommand {
    // 1.创建命令
    RACCommand *command = [[RACCommand alloc] initWithSignalBlock:^RACSignal * _Nonnull(id  _Nullable input) {
        NSLog(@"putong - input为执行命令传进来的参数: %@", input);
        // 这里的返回值不允许为nil
        return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
            [subscriber sendNext:@"执行命令产生的数据"];
            return nil;
        }];
    }];
    
    // 2.执行命令
    RACSignal *signal = [command execute:@1];   // 这里其实用到的是replaySubject 可以先发送命令再订阅
    [signal subscribeNext:^(id  _Nullable x) {
        NSLog(@"putong - 如何拿到执行命令中产生的数据呢？直接订阅执行命令返回的信号: 【%@】", x);
    }];
}

- (void)yibanCommand {
    RACCommand *command = [[RACCommand alloc] initWithSignalBlock:^RACSignal * _Nonnull(id  _Nullable input) {
        NSLog(@"yiban - input为执行命令传进来的参数: %@", input);
        return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
            [subscriber sendNext:@"执行命令产生的数据"];
            return nil;
        }];
    }];
    
    // 注意: 这里必须是先订阅才能发送命令
    [command.executionSignals subscribeNext:^(RACSignal * _Nullable x) {
        // executionSignals: 信号源，信号中信号
        [x subscribeNext:^(id  _Nullable x) {
            NSLog(@"yiban - 如何拿到执行命令中产生的数据呢？订阅信号: 【%@】", x);
        }];
    }];
    
    [command execute:@2];
}

- (void)gaojiCommand {
    RACCommand *command = [[RACCommand alloc] initWithSignalBlock:^RACSignal * _Nonnull(id  _Nullable input) {
        NSLog(@"gaoji - input为执行命令传进来的参数: %@", input);
        return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
            [subscriber sendNext:@"执行命令产生的数据"];
            return nil;
        }];
    }];
    
    // switchToLatest获取最新发送的信号，只能用于信号中信号。
    [command.executionSignals.switchToLatest subscribeNext:^(id  _Nullable x) {
        NSLog(@"gaoji - 如何拿到执行命令中产生的数据呢？switchToLatest获取最新发送的信号: 【%@】", x);
    }];
  
    [command execute:@3];
}


/// switchToLatest 用于信号中信号
- (void)switchToLatest {
    // 创建信号中信号 (signalofsignals:信号，发送数据就是信号)
    RACSubject *signalofsignals = [RACSubject subject];
    RACSubject *signalA = [RACSubject subject];
    
    // 订阅信号
    [signalofsignals subscribeNext:^(RACSignal * _Nullable x) {
        [x subscribeNext:^(id  _Nullable x) {
            NSLog(@"switchToLatestDemo - %@", x);
        }];
    }];
    
    // switchToLatest: 获取信号中信号发送的最新信号
    [signalofsignals.switchToLatest subscribeNext:^(id  _Nullable x) {
        NSLog(@"switchToLatestDemo - 获取信号中信号发送的最新信号: %@", x);
    }];
    
    // 发送信号
    [signalofsignals sendNext:signalA];
    [signalA sendNext:@4];
}

/// 监听事件有没有完成
- (void)racCommand {
    // 注意: 当前命令内部发送数据完成，一定要主动发送完成
    RACCommand *command = [[RACCommand alloc] initWithSignalBlock:^RACSignal * _Nonnull(id  _Nullable input) {
        NSLog(@"command - input为执行命令传进来的参数: %@", input);
        return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
            [subscriber sendNext:@"执行命令产生的数据"];
            [subscriber sendCompleted];
            return nil;
        }];
    }];
    
    // 监听事件有没有完成
    [command.executing subscribeNext:^(NSNumber * _Nullable x) {
        if ([x boolValue] == YES) {
            NSLog(@"command - 正在执行(%@)", x);
        } else {
            NSLog(@"command - 执行完成/没有执行(%@)", x);
        }
    }];
    
    [command execute:@"执行"];
}

@end
