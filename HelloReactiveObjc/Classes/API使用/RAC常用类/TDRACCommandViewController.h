//
//  TDRACCommandViewController.h
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//  RACCommand:RAC中用于处理事件的类，可以把事件如何处理,事件中的数据如何传递，包装到这个类中，他可以很方便的监控事件的执行过程

#import "TDRACBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TDRACCommandViewController : TDRACBaseViewController

@end

NS_ASSUME_NONNULL_END
