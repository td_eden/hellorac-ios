//
//  TDRACSubjectViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//  信号提供者，自己可以充当信号，又能发送信号 使用场景:通常用来代替代理，有了它，就不必要定义代理了

#import "TDRACSubjectViewController.h"

@interface TDRACSubjectViewController ()

@end

@implementation TDRACSubjectViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = TDRACSubject;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // 当退出当前控制器时触发
    [self.subject sendNext:@"Hello RACSubject!"];
}

/// RACSubject(可发送信号也可以订阅信号)
- (void)racSubject {
    // 创建RACSubject
    RACSubject *subject = [RACSubject subject];
    // 订阅信号
    [subject subscribeNext:^(id  _Nullable x) {
        NSLog(@"%@", x);
    }];
    // 发送信号
    [subject sendNext:@"信号🍺🍺🍺🍺🍺🍺🍺"];
}

@end

/**
 Tip:
 RACSubject 和 RACReplaySubject 的区别
 RACSubject 必须要先订阅信号之后才能发送信号， 而 RACReplaySubject 可以先发送信号后订阅.
 */
