//
//  TDRACMulticastConnectionViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//

#import "TDRACMulticastConnectionViewController.h"

@interface TDRACMulticastConnectionViewController ()

@end

@implementation TDRACMulticastConnectionViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = TDRACMulticastConnection;
    [self setupTipLabel];
    
    [self racMulticastConnection];
}

/// 使用RACMulticastConnection，无论有多少个订阅者，无论订阅多少次，我只发送一个
- (void)racMulticastConnection {
    // 1.发送请求。用一个信号内包装，不管有多少个订阅者，只想发一次请求
    RACSignal *signal = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        NSLog(@"afn发送请求啦");
        [subscriber sendNext:@"发送信号"];
        [subscriber sendCompleted];
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"signal销毁了");
        }];
    }];
    
    // 2.创建连接类
    RACMulticastConnection *connection = [signal publish];
    [connection.signal subscribeNext:^(id  _Nullable x) {
        NSLog(@"subscribeNext-->1");
    }];
    [connection.signal subscribeNext:^(id  _Nullable x) {
        NSLog(@"subscribeNext-->2");
    }];
    [connection.signal subscribeNext:^(id  _Nullable x) {
        NSLog(@"subscribeNext-->3");
    }];
    
    // 3.连接。只有连接了才会把信号源变为热信号
    [connection connect];
}

/*
/// 请用如上写法
- (void)errorDemo {
    // 普通写法, 这样的缺点是：没订阅一次信号就得重新创建并发送请求，这样很不友好
    RACSignal *signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        // didSubscribeblock中的代码都统称为副作用。
        // 发送请求---比如afn
        NSLog(@"发送请求啦");
        // 发送信号
        [subscriber sendNext:@"ws"];
        return nil;
    }];
    [signal subscribeNext:^(id x) {
        NSLog(@"%@", x);
    }];
    [signal subscribeNext:^(id x) {
        NSLog(@"%@", x);
    }];
    [signal subscribeNext:^(id x) {
        NSLog(@"%@", x);
    }];
}
*/

- (void)setupTipLabel {
    UILabel *tipLabel = [[UILabel alloc] initWithFrame:self.view.bounds];
    tipLabel.text = @"当有多个订阅者，但是我们只想发送一个信号的时候怎么办？这时我们就可以用RACMulticastConnection";
    tipLabel.textAlignment = NSTextAlignmentCenter;
    tipLabel.numberOfLines = 0;
    [self.view addSubview:tipLabel];
}

@end
