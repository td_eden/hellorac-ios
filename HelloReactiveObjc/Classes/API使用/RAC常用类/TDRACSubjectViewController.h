//
//  TDRACSubjectViewController.h
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//  信号提供者，自己可以充当信号，又能发送信号 使用场景:通常用来代替代理，有了它，就不必要定义代理了

#import "TDRACBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TDRACSubjectViewController : TDRACBaseViewController

@property (nonatomic, strong) RACSubject *subject;

@end

NS_ASSUME_NONNULL_END
