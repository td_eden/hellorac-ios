//
//  TDRACDelegateViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//

#import "TDRACDelegateViewController.h"

@interface TDRACDelegateViewController ()

@end

@implementation TDRACDelegateViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = TDRACDelegate;
    
    CGRect btnFrame = CGRectMake((UIScreen.width-100)*0.5, (UIScreen.height-40)*0.5, 100, 40);
    UIButton *button = [[UIButton alloc] initWithFrame:btnFrame];
    [button setTitle:@"按钮点击" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor lightGrayColor];
    [button addTarget:self action:@selector(btnClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    [self racDelegate];
}

#pragma mark - 代替代理

- (void)racDelegate {
    @weakify(self)
    // 监听按钮点击方法的信号
    // 当执行完btnClickAction后会执行此订阅
    [[self rac_signalForSelector:@selector(btnClickAction:)] subscribeNext:^(RACTuple * _Nullable x) {
        // RACTuple: 元组类,类似NSArray,用来包装值
        /**
         <RACTuple: 0x600000af1c70> (
             "<UIButton: 0x7fd9c390d980; frame = (137.5 313.5; 100 40); opaque = NO; layer = <CALayer: 0x600000888b20>>"
         )
         */
        @strongify(self)
        [self showAlertControllerWithTitle:@"代替代理" message:@"监听按钮点击方法的信号。" actionTitles:nil cancelTitle:@"取消" preferredStyle:UIAlertControllerStyleAlert callBack:nil];
    }];
}

- (void)btnClickAction:(UIButton *)sender {
    NSLog(@"按钮点击了");
}

@end
