//
//  TDRACTimeViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//

#import "TDRACTimeViewController.h"

@interface TDRACTimeViewController ()

@property (nonatomic, strong) RACDisposable *disposable;
@property (nonatomic, strong) UILabel *label1;

@end

@implementation TDRACTimeViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
    [self.disposable dispose];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = TDRACTime;
    
    CGRect label1Rect = CGRectMake(16, (UIScreen.height-100)/2, UIScreen.width-16*2, 100);
    self.label1 = [[UILabel alloc] initWithFrame:label1Rect];
    self.label1.numberOfLines = 0;
    self.label1.text = @"类似timer会延迟2秒开始执行哦~";
    [self.view addSubview:self.label1];
    
    [self racTime];
}

#pragma mark - 计时器（interval、delay）

- (void)racTime {
    // RACScheduler: RAC中的队列，用GCD封装的
    __block int count = 1;
    // 类似timer
    @weakify(self)
    self.disposable = [[RACSignal interval:2 onScheduler:[RACScheduler mainThreadScheduler]] subscribeNext:^(NSDate * _Nullable x) {    // x是当前时间
        @strongify(self)
        self.label1.text = [NSString stringWithFormat:@"定时器第%i次执行，时间：%@", count, x];
        
        if (count >= 10) {
            [self.disposable dispose];  // 关闭定时器
        }
        count ++;
    }];
    
    // 延时
    [[[RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [subscriber sendNext:@"延时2秒"];
        return nil;
    }] delay:2] subscribeNext:^(id  _Nullable x) {
        NSLog(@"%@", x);
    }];
}

@end
