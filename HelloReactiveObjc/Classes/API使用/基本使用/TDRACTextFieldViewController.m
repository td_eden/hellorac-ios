//
//  TDRACTextFieldViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/6.
//

#import "TDRACTextFieldViewController.h"

@interface TDRACTextFieldViewController ()

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UILabel *label1;
@property (nonatomic, strong) UILabel *label2;
@property (nonatomic, strong) UILabel *label3;

@end

@implementation TDRACTextFieldViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = TDRACTextField;
 
    CGRect textRect = CGRectMake((UIScreen.width-200)/2, (UIScreen.height-40)/2-200, 200, 40);
    self.textField = [[UITextField alloc] initWithFrame:textRect];
    self.textField.borderStyle = UITextBorderStyleRoundedRect;
    [self.view addSubview:self.textField];
    
    CGRect label1Rect = CGRectMake(16, CGRectGetMaxY(self.textField.frame)+20, UIScreen.width-16*2, 20);
    self.label1 = [[UILabel alloc] initWithFrame:label1Rect];
    [self.view addSubview:self.label1];
    
    CGRect label2Rect = CGRectMake(16, CGRectGetMaxY(self.label1.frame)+20, UIScreen.width-16*2, 20);
    self.label2 = [[UILabel alloc] initWithFrame:label2Rect];
    [self.view addSubview:self.label2];
    
    CGRect label3Rect = CGRectMake(16, CGRectGetMaxY(self.label2.frame)+20, UIScreen.width-16*2, 20);
    self.label3 = [[UILabel alloc] initWithFrame:label3Rect];
    [self.view addSubview:self.label3];
    
    [self racTextField];
}

#pragma mark - UITextField

- (void)racTextField {
    @weakify(self)
    // 监听文本输入
    [[self.textField rac_textSignal] subscribeNext:^(NSString * _Nullable x) {
        @strongify(self)
        self.label1.text = [NSString stringWithFormat:@"监听文本输入: %@", x];
    }];
    
    // 可根据自己想要监听的事件选择
    [[self.textField rac_signalForControlEvents:UIControlEventEditingChanged] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self)
        self.label2.text = [NSString stringWithFormat:@"监听事件EditingChanged: %@", ((UITextField *)x).text];
    }];
    
    // 添加条件 -- 下面表示输入文字长度 > 10 时才会调用subscribeNext
    [[self.textField.rac_textSignal filter:^BOOL(NSString * _Nullable value) {
        return value.length > 10;
    }] subscribeNext:^(NSString * _Nullable x) {
        @strongify(self)
        self.label3.text = [NSString stringWithFormat:@"监听条件 文字长度>10 时: %@", x];
    }];
}

@end
