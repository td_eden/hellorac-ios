//
//  TDRACPropertyViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//

#import "TDRACPropertyViewController.h"

@interface TDRACPropertyViewController ()

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UILabel *label1;

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UILabel *label2;

@end

@implementation TDRACPropertyViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = TDRACProperty;
        
    self.name = @"初始值";
    
    CGRect btnRect = CGRectMake((UIScreen.width-200)/2, (UIScreen.height-40)/2-200, 200, 40);
    self.button = [[UIButton alloc] initWithFrame:btnRect];
    [self.button setTitle:@"点击改变属性值" forState:UIControlStateNormal];
    [self.button setBackgroundColor:[UIColor brownColor]];
    [self.view addSubview:self.button];
    [self racButton];
    
    CGRect label1Rect = CGRectMake(16, CGRectGetMaxY(self.button.frame)+20, UIScreen.width-16*2, 40);
    self.label1 = [[UILabel alloc] initWithFrame:label1Rect];
    self.label1.numberOfLines = 0;
    self.label1.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.label1];
    
    CGRect textRect = CGRectMake((UIScreen.width-200)/2, (UIScreen.height-40)/2, 200, 40);
    self.textField = [[UITextField alloc] initWithFrame:textRect];
    self.textField.borderStyle = UITextBorderStyleLine;
    [self.view addSubview:self.textField];
    
    CGRect label2Rect = CGRectMake(16, CGRectGetMaxY(self.textField.frame)+20, UIScreen.width-16*2, 20);
    self.label2 = [[UILabel alloc] initWithFrame:label2Rect];
    self.label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.label2];
    
    [self racProperty];
}

- (void)racButton {
    @weakify(self)
    [[self.button rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self)
        self.name = [self.name isEqualToString: @"初始值"] ? @"新的值" : @"初始值";
    }];
}

#pragma mark - 监听属性变化

- (void)racProperty {
    // 监听self的name属性
    @weakify(self)
    [RACObserve(self, name) subscribeNext:^(id  _Nullable x) {
        @strongify(self)
        self.label1.text = (NSString *)x;
    }];
    [[self rac_valuesForKeyPath:@"name" observer:self] subscribeNext:^(id  _Nullable x) {
        NSLog(@"属性的改变: %@", x);
    }];
    
    // 此处RAC宏相当于让_label订阅了_textField的文本变化信号
    // 赋值给label的text属性
    RAC(_label2, text) = _textField.rac_textSignal;
}

@end
