//
//  TDRACIterateViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//

#import "TDRACIterateViewController.h"

@interface TDRACIterateViewController ()

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UILabel *label1;

@end

@implementation TDRACIterateViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = TDRACIterate;
    
    CGRect tipLabelRect = CGRectMake(16, (UIScreen.height-40)/2-200, UIScreen.width-16*2, 20);
    UILabel *tipLabel = [[UILabel alloc] initWithFrame:tipLabelRect];
    tipLabel.text = @"请输入待split的字符串，以\"-\"来分割：";
    [self.view addSubview:tipLabel];
    
    CGRect fieldRect = CGRectMake(16, CGRectGetMaxY(tipLabel.frame)+15, UIScreen.width-(16*2)-100, 40);
    UITextField *field = [[UITextField alloc] initWithFrame:fieldRect];
    field.borderStyle = UITextBorderStyleRoundedRect;
    [self.view addSubview:field];
    self.textField = field;
    
    CGRect splitRect = CGRectMake(field.frame.size.width+16+10, CGRectGetMaxY(tipLabel.frame)+15, 80, 40);
    UIButton *splitBtn = [[UIButton alloc] initWithFrame:splitRect];
    splitBtn.backgroundColor = [UIColor brownColor];
    [splitBtn setTitle:@"开始Split" forState:UIControlStateNormal];
    [self.view addSubview:splitBtn];
    
    @weakify(self)
    [[splitBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        //NSLog(@"splitBtn rac_signal thread: %@", [NSThread currentThread]); // main
        
        @strongify(self) //**********
        NSArray *array = [self.textField.text componentsSeparatedByString:@"-"];
        [self racIterate:array]; //**********
    }];
    
    
    CGRect label1Rect = CGRectMake(16, CGRectGetMaxY(self.textField.frame)+20, UIScreen.width-16*2, 40);
    self.label1 = [[UILabel alloc] initWithFrame:label1Rect];
    self.label1.numberOfLines = 0;
    [self.view addSubview:self.label1];
}

#pragma mark - 遍历数组和字典

- (void)racIterate:(NSArray *)array {
    // 遍历数组
    //NSArray *array = @[@"1", @"2", @"3", @"4", @"5"];
    NSMutableString *mutableString = [[NSMutableString alloc] init];
    [array.rac_sequence.signal subscribeNext:^(id  _Nullable x) {
        //NSLog(@"array rac_sequence thread: %@", [NSThread currentThread]);  // background
        [mutableString appendFormat:@"%@\n", x];
    } completed:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            self.label1.text = mutableString;
            [self.label1 sizeToFit];
        });
    }];
}

@end
