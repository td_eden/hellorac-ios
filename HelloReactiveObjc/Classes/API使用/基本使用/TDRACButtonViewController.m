//
//  TDRACButtonViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//

#import "TDRACButtonViewController.h"

@interface TDRACButtonViewController ()

@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UILabel *label1;

@end

@implementation TDRACButtonViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = TDRACButton;
    
    CGRect btnRect = CGRectMake((UIScreen.width-200)/2, (UIScreen.height-40)/2-200, 200, 40);
    self.button = [[UIButton alloc] initWithFrame:btnRect];
    [self.button setTitle:@"按  钮" forState:UIControlStateNormal];
    [self.button setBackgroundColor:[UIColor brownColor]];
    [self.view addSubview:self.button];
    
    CGRect label1Rect = CGRectMake(16, CGRectGetMaxY(self.button.frame)+20, UIScreen.width-16*2, 100);
    self.label1 = [[UILabel alloc] initWithFrame:label1Rect];
    self.label1.numberOfLines = 0;
    [self.view addSubview:self.label1];
    
    [self racButton];
}

#pragma mark - UIButton

- (void)racButton {
    // 监听按钮点击事件
    @weakify(self)
    [[self.button rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self)
        self.label1.text = [NSString stringWithFormat:@"监听%@点击: %@", ((UIButton *)x).titleLabel.text, x];
    }];
}

@end
