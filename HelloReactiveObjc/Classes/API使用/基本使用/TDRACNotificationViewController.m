//
//  TDRACNotificationViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//

#import "TDRACNotificationViewController.h"

@interface TDRACNotificationViewController ()

@end

@implementation TDRACNotificationViewController

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = TDRACNotification;
    
    CGRect btnFrame = CGRectMake((UIScreen.width-100)*0.5, (UIScreen.height-40)*0.5, 100, 40);
    UIButton *postNotificationBtn = [[UIButton alloc] initWithFrame:btnFrame];
    [postNotificationBtn setTitle:@"发送通知" forState:UIControlStateNormal];
    postNotificationBtn.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:postNotificationBtn];
    [[postNotificationBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"notification" object:nil userInfo:nil];
    }];
    
    [self racNotification];
}

#pragma mark - 监听 Notification 通知事件

- (void)racNotification {
    @weakify(self)
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:@"notification" object:nil] subscribeNext:^(NSNotification * _Nullable x) {
        @strongify(self)
        [self showAlertControllerWithTitle:x.name message:@"监听到你触发了通知。" actionTitles:nil cancelTitle:@"取消" preferredStyle:UIAlertControllerStyleActionSheet callBack:nil];
    }];
}

@end
