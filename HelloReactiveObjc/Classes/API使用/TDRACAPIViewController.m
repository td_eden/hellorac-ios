//
//  TDRACAPIViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/5.
//

#import "TDRACAPIViewController.h"
// 基本使用
#import "TDRACTextFieldViewController.h"
#import "TDRACButtonViewController.h"
#import "TDRACTimeViewController.h"
#import "TDRACPropertyViewController.h"
#import "TDRACIterateViewController.h"
#import "TDRACNotificationViewController.h"
#import "TDRACDelegateViewController.h"
// RAC常用类
#import "TDRACSignalViewController.h"
#import "TDRACSubjectViewController.h"
#import "TDRACTupleViewController.h"
#import "TDRACMulticastConnectionViewController.h"
#import "TDRACCommandViewController.h"
// 高级函数使用
#import "TDRACFuncListViewController.h"
// MVVM+RAC实战
#import "TDRACDemoListViewController.h"

@interface TDRACAPIViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataSource;

@end

@implementation TDRACAPIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"ReactiveObjC使用教程";
    
    /*
    if (@available(iOS 15.0, *)) {
        UINavigationBarAppearance *barApp = [UINavigationBarAppearance new];
        self.navigationController.navigationBar.scrollEdgeAppearance = barApp;
        self.navigationController.navigationBar.standardAppearance = barApp;
    } else {
        // Fallback on earlier versions
    }*/
    
    //[self.tableView addSubview:[self setupFooterView]];
    self.tableView.tableFooterView = [self setupFooterView];
    [self.view addSubview:self.tableView];
}

- (UIView *)setupFooterView {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreen.width, 66)];
    footerView.backgroundColor = UIColor.groupTableViewBackgroundColor;
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:footerView.bounds];
    headerLabel.text = @"更多信息请参照: \n https://juejin.cn/post/7034062046774132773";
    headerLabel.numberOfLines = 0;
    headerLabel.textColor = UIColor.grayColor;
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.font = [UIFont systemFontOfSize:13];
    [footerView addSubview:headerLabel];
    
    return footerView;
}

#pragma mark - 基本使用

- (void)pushBasicUseViewControllerWithTitle:(NSString *)title {
    UIViewController *vc = nil;
    if ([title isEqualToString:TDRACTextField]) {
        vc = [[TDRACTextFieldViewController alloc] init];
    } else if ([title isEqualToString:TDRACButton]) {
        vc = [[TDRACButtonViewController alloc] init];
    } else if ([title isEqualToString:TDRACTime]) {
        vc = [[TDRACTimeViewController alloc] init];
    } else if ([title isEqualToString:TDRACProperty]) {
        vc = [[TDRACPropertyViewController alloc] init];
    } else if ([title isEqualToString:TDRACIterate]) {
        vc = [[TDRACIterateViewController alloc] init];
    } else if ([title isEqualToString:TDRACNotification]) {
        vc = [[TDRACNotificationViewController alloc] init];
    } else if ([title isEqualToString:TDRACDelegate]) {
        vc = [[TDRACDelegateViewController alloc] init];
    }
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - RAC常用类

- (void)pushClassUseViewControllerWithTitle:(NSString *)title {
    UIViewController *vc = nil;
    if ([title isEqualToString:TDRACSignal]) {
        vc = [[TDRACSignalViewController alloc] init];
    } else if ([title isEqualToString:TDRACSubject]) {
        TDRACSubjectViewController *subjectVC = [[TDRACSubjectViewController alloc] init];
        subjectVC.subject = RACSubject.subject;
        [subjectVC.subject subscribeNext:^(id  _Nullable x) { // 这里的循环引用 ？？？
            [self showAlertControllerWithTitle:@"RACSubject" message:[NSString stringWithFormat:@"Subject控制器发送了:%@", x] actionTitles:nil cancelTitle:@"取消" preferredStyle:UIAlertControllerStyleAlert callBack:nil];
        }];
        vc = subjectVC;
    } else if ([title isEqualToString:TDRACTuple]) {
        vc = [[TDRACTupleViewController alloc] init];
    } else if ([title isEqualToString:TDRACMulticastConnection]) {
        vc = [[TDRACMulticastConnectionViewController alloc] init];
    } else if ([title isEqualToString:TDRACCommand]) {
        vc = [[TDRACCommandViewController alloc] init];
    }
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 高级函数使用

- (void)pushFuncUseViewControllerWithTitle:(NSString *)title {
    TDRACFuncListViewController *listVC = [[TDRACFuncListViewController alloc] init];
    listVC.title = title;
    [self.navigationController pushViewController:listVC animated:YES];
}

#pragma mark - RAC+MVVM（项目实战）

- (void)pushRacUseViewControllerWithTitle:(NSString *)title {
    UIViewController *vc = [[TDRACDemoListViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *textString = self.dataSource[indexPath.section][indexPath.row];
    switch (indexPath.section) {
        case 0: // 基本使用
            [self pushBasicUseViewControllerWithTitle:textString];
            break;
        case 1: // RAC常用类
            [self pushClassUseViewControllerWithTitle:textString];
            break;
        case 2: // 高级函数使用
            [self pushFuncUseViewControllerWithTitle:textString];
            break;
        default:// RAC+MVVM（项目实战）
            [self pushRacUseViewControllerWithTitle:textString];
            break;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ((NSArray *)self.dataSource[section]).count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = self.dataSource[indexPath.section][indexPath.row];
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"基本使用";
            break;
        case 1:
            return @"RAC常用类";
            break;
        case 2:
            return @"高级函数使用";
            break;
        default:
            return @"RAC+MVVM（项目实战）";
            break;
    }
}

#pragma mark - Lazy loading

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorInset = UIEdgeInsetsMake(0, 12, 0, -12);
    }
    return _tableView;
}

- (NSArray *)dataSource {
    if (!_dataSource) {
        
        NSArray *basicArray = @[TDRACTextField, TDRACButton, TDRACTime, TDRACProperty, TDRACIterate, TDRACNotification, TDRACDelegate];
        
        NSArray *classArray = @[TDRACSignal, TDRACSubject, TDRACTuple, TDRACMulticastConnection, TDRACCommand];
        
        NSArray *funcArray = @[TDRACCombination, TDRACMapping, TDRACFiltration];
        
        NSArray *demoArray = @[TDRACMVVMDemo];
        
        _dataSource = @[basicArray, classArray, funcArray, demoArray];
    }
    return _dataSource;
}

@end
