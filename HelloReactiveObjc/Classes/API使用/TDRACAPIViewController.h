//
//  TDRACAPIViewController.h
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/5.
//  TDRACAPIViewController

#import "TDRACBaseViewController.h"

@interface TDRACAPIViewController : TDRACBaseViewController

/**
 #### ReactiveCocoa常见类
 - RACSiganl: 信号类
 - RACSubscriber: 表示订阅者的意思，用于发送信号，这是一个协议
 - RACDisposable: 用于取消订阅或者清理资源，当信号发送完成或者发送错误的时候，就会自动触发它
 - RACSubject: 信号提供者，自己可以充当信号，又能发送信号 使用场景:通常用来代替代理，有了它，就不必要定义代理了
 - RACSequence: RAC中的集合类，用于代替NSArray,NSDictionary,可以使用它来快速遍历数组和字典
 - RACCommand: RAC中用于处理事件的类，可以把事件如何处理,事件中的数据如何传递，包装到这个类中，他可以很方便的监控事件的执行过程
 - RACMulticastConnection: 用于当一个信号，被多次订阅时，为了保证创建信号时，避免多次调用创建信号中的block
 - RACTuple: 元组类,类似NSArray,用来包装值
 - RACScheduler: RAC中的队列，用GCD封装的
 - RACUnit: 表⽰stream不包含有意义的值,也就是看到这个，可以直接理解为nil
 - RACEvent: 把数据包装成信号事件(signal event)。它主要通过RACSignal的-materialize来使用
 
 
 #### ReactiveCocoa常见宏
 - RAC(TARGET, [KEYPATH, [NIL_VALUE]]): 用于给某个对象的某个属性绑定
 - RACObserve(self, name): 监听某个对象的某个属性,返回的是信号
 - RACTuplePack: 把数据包装成RACTuple（元组类）
 - RACTupleUnpack: 把RACTuple（元组类）解包成对应的数据
 
 
 #### ReactiveCocoa常见用法
 - rac_signalForSelector: 用于替代代理。
 - rac_valuesAndChangesForKeyPath: 代替KVO，用于监听某个对象的属性改变
 - rac_signalForControlEvents: 用于监听某个事件
 - rac_addObserverForName: 用于监听某个通知
 - rac_textSignal: 监听文本框文字改变
 - rac_liftSelector:withSignalsFromArray:Signals:
 */

@end

