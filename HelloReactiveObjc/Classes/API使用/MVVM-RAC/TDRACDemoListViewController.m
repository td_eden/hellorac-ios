//
//  TDRACDemoListViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/11/17.
//

#import "TDRACDemoListViewController.h"

#import "TDRACLoginViewController01.h"
#import "TDRACLoginViewController02.h"

@interface TDRACDemoListViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataSource;

@end

@implementation TDRACDemoListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = TDRACMVVMDemo;
    
    [self.view addSubview:self.tableView];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *textString = self.dataSource[indexPath.section][indexPath.row];
    
    UIViewController *viewCtrl = nil;
    // 登录示例
    if ([textString isEqualToString:@"LoginViewController01"]) {
        viewCtrl = [[TDRACLoginViewController01 alloc] initWithNibName:@"TDRACLoginViewController01" bundle:nil];
    } else if ([textString isEqualToString:@"LoginViewController02"]) {
        viewCtrl = [[TDRACLoginViewController02 alloc] initWithNibName:@"TDRACLoginViewController02" bundle:nil];
    }
    
    [self.navigationController pushViewController:viewCtrl animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ((NSArray *)self.dataSource[section]).count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = self.dataSource[indexPath.section][indexPath.row];
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"登录示例";
    } else {
        return @"MVVM+RAC实战";
    }
}

#pragma mark - Lazy loading

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorInset = UIEdgeInsetsMake(0, 12, 0, -12);
    }
    return _tableView;
}

- (NSArray *)dataSource {
    if (!_dataSource) {
        
        NSArray *loginDemoArray = @[@"LoginViewController01", @"LoginViewController02"];
        
        _dataSource = @[loginDemoArray];
    }
    return _dataSource;
}

@end
