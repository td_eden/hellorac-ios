//
//  TDRACLoginViewController02.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/11/16.
//

#import "TDRACLoginViewController02.h"
#import "TDRACLoginViewModel.h"

#import "TDRACRequestViewModel.h"

@interface TDRACLoginViewController02 ()

@property (weak, nonatomic) IBOutlet UITextField *accountField;
@property (weak, nonatomic) IBOutlet UITextField *pwdField;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;

@property (nonatomic, strong) TDRACLoginViewModel *loginVM;

@property (nonatomic, strong) TDRACRequestViewModel *requestVM;

@end

@implementation TDRACLoginViewController02

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // MVVM:开发中先创建VM，把业务逻辑处理好，然后在控制器里执行
    [self bindViewModel];
    [self loginEvent];
    
    
    
    // test GET https://api.douban.com/v2/book/search
    RACSignal *signal = [self.requestVM.requestCommand execute:nil];
    [signal subscribeNext:^(id x) {
        NSLog(@"发送请求: %@", x);
    }];
}

- (void)bindViewModel {
    RAC(self.loginVM, account) = self.accountField.rac_textSignal;
    RAC(self.loginVM, pwd) = self.pwdField.rac_textSignal;
}

- (void)loginEvent {
    // 设置按钮是否能点击
    RAC(self.loginBtn, enabled) = self.loginVM.loginEnableSignal;
    // 监听登录按钮点击
    [[self.loginBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self.loginVM.loginCommand execute:nil];
    }];
}


#pragma mark - 懒加载

- (TDRACLoginViewModel *)loginVM {
    if (!_loginVM) {
        _loginVM = [[TDRACLoginViewModel alloc] init];
    }
    return _loginVM;
}

- (TDRACRequestViewModel *)requestVM {
    if (!_requestVM) {
        _requestVM = [[TDRACRequestViewModel alloc] init];
    }
    return _requestVM;
}

@end
