//
//  TDRACLoginViewModel.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/11/16.
//

#import "TDRACLoginViewModel.h"

@implementation TDRACLoginViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    // 1.处理登录按钮是否可点击的信号
    _loginEnableSignal = [RACSignal combineLatest:@[RACObserve(self, account), RACObserve(self, pwd)] reduce:^id (NSString *account, NSString *pwd){
        return @(account.length && pwd.length);
    }];
    
    // 2.处理登录按钮点击命令
    _loginCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal * _Nonnull(id  _Nullable input) {
        return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [subscriber sendNext:@"发送登录的数据"];
                [subscriber sendCompleted];
            });
           
            return nil;
        }];
    }];
    
    // 3.处理登录的请求返回的结果  获取命令中的信号源
    [_loginCommand.executionSignals.switchToLatest subscribeNext:^(id  _Nullable x) {
        NSLog(@"获取命令中的信号源: %@", x);
    }];
    
    // 4.处理登录执行过程
    [[_loginCommand.executing skip:1] subscribeNext:^(NSNumber * _Nullable x) { // 跳过第一步（没有执行这步）
        if ([x boolValue] == YES) {
            NSLog(@"正在执行，显示蒙版");
        } else {
            NSLog(@"执行完成，取消蒙版");
        }
    }];
}

@end
