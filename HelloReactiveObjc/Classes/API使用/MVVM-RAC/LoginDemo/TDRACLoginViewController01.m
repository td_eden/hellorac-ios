//
//  TDRACLoginViewController01.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/11/16.
//

#import "TDRACLoginViewController01.h"

@interface TDRACLoginViewController01 ()

@property (weak, nonatomic) IBOutlet UITextField *accountField;
@property (weak, nonatomic) IBOutlet UITextField *pwdField;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;

@end

@implementation TDRACLoginViewController01

- (void)dealloc {
    NSLog(@"%s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // MVVM:
    // VM:视图模型----处理展示的业务逻辑
    // 每一个控制器都对应一个VM模型
    
    RACSignal *loginEnableSignal = [RACSignal combineLatest:@[self.accountField.rac_textSignal, self.pwdField.rac_textSignal] reduce:^id (NSString *account, NSString *pwd){
        return @(account.length && pwd.length);
    }];
    // 设置按钮是否能点击
    RAC(self.loginBtn, enabled) = loginEnableSignal;
    
    
    // 创建登录命令
    RACCommand *command = [[RACCommand alloc] initWithSignalBlock:^RACSignal * _Nonnull(id  _Nullable input) {
        // block 调用：执行命令就会调用
        // block 作用: 事件处理
        // 发送登录请求
        return [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                // 发送数据
                [subscriber sendNext:@"发送登录的数据"];
                [subscriber sendCompleted];
            });
            
            return nil;
        }];
    }];
    // 获取命令中的信号源
    [command.executionSignals.switchToLatest subscribeNext:^(id  _Nullable x) {
        NSLog(@"获取命令中的信号源: %@", x);
    }];
    // 监听命令执行过程
    [[command.executing skip:1] subscribeNext:^(NSNumber * _Nullable x) { // 跳过第一步（没有执行这步）
        if ([x boolValue] == YES) {
            NSLog(@"正在执行，显示蒙版");
        } else {
            NSLog(@"执行完成，取消蒙版");
        }
    }];
    // 监听登录按钮点击
    [[self.loginBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [command execute:nil];  // 处理登录事件
    }];
}

@end
