//
//  TDRACLoginViewModel.h
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/11/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TDRACLoginViewModel : NSObject

/// 处理按钮是否允许点击
@property (nonatomic, strong, readonly) RACSignal *loginEnableSignal;

/// 保存登录账号
@property (nonatomic, copy) NSString *account;
/// 保存登录密码
@property (nonatomic, copy) NSString *pwd;

/// 登录按钮的命令
@property (nonatomic, strong, readonly) RACCommand *loginCommand;

@end

NS_ASSUME_NONNULL_END
