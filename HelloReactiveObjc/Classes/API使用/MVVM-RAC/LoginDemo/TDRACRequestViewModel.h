//
//  TDRACRequestViewModel.h
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/11/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TDRACRequestViewModel : NSObject

@property (nonatomic, strong, readonly) RACCommand *requestCommand;

@end

NS_ASSUME_NONNULL_END
