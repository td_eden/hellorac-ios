//
//  UIViewController+AlertController.h
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (AlertController)

/// 弹窗
/// @param title 标题
/// @param message 消息
/// @param titles 事件按钮标题数组
/// @param cancelTitle 事件按钮取消标题
/// @param preferredStyle 样式
/// @param callBack 事件回调
- (void)showAlertControllerWithTitle:(nullable NSString *)title message:(nullable NSString *)message actionTitles:(nullable NSArray<NSString *> *)titles cancelTitle:(nullable NSString *)cancelTitle preferredStyle:(UIAlertControllerStyle)preferredStyle callBack:(nullable void (^)(NSInteger index))callBack;

@end

NS_ASSUME_NONNULL_END
