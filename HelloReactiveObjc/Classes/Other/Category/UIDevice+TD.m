//
//  UIDevice+TD.m
//  UIDevice
//
//  Created by 李同德 on 2020/7/1.
//  Copyright © 2020 李同德. All rights reserved.
//

#import "UIDevice+TD.h"
#import <sys/sysctl.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#import <sys/utsname.h>

@implementation UIDevice (TD)

+ (CGFloat)navigationHeight
{
    return [self navigationBarHeight] + [self statusBarHeight];
}

+ (CGFloat)navigationBarHeight
{
    return 44.f;
}

+ (CGFloat)statusBarHeight
{
    static CGFloat statusBarHeight = 20;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    });
    return statusBarHeight;
}

+ (CGFloat)screenWidth
{
    static CGFloat screenWidth = 0;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        screenWidth = [UIScreen mainScreen].bounds.size.width;
    });
    return screenWidth;
}

+ (CGFloat)screenHeight
{
    static CGFloat screenHeight = 0;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        screenHeight = [UIScreen mainScreen].bounds.size.height;
    });
    return screenHeight;
}

+ (CGFloat)tabBarHeight
{
    return [[self class] safeAreaInset].bottom + 49.f;
}

+ (BOOL)isIPhoneXSeries
{
    static BOOL isiPhoneX = NO;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (UIDevice.statusBarHeight == 40) {
            isiPhoneX = YES;
        } else {
#if TARGET_IPHONE_SIMULATOR
            NSString *model = NSProcessInfo.processInfo.environment[@"SIMULATOR_MODEL_IDENTIFIER"];
#else
            struct utsname systemInfo;
            uname(&systemInfo);
            NSString *model = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
#endif
            NSArray *infos = [model componentsSeparatedByString:@","];
            NSString *phoneName = [infos firstObject];
            NSString *phoneNo = [phoneName stringByReplacingOccurrencesOfString:@"iPhone" withString:@""];
            if (phoneNo.intValue > 10) {
                isiPhoneX = YES;
            } else {
                NSString *modelNum = [infos lastObject];
                if (phoneNo.intValue == 10 && (modelNum.intValue == 6 || modelNum.intValue == 3 )) {
                    isiPhoneX = YES;
                }
            }
        }
    });
    return isiPhoneX;
}

@end


@implementation UIDevice (TDHardware)

+ (NSString *)modelVersion
{
    static NSString *_modelVersion = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        size_t size;
        sysctlbyname("hw.machine", NULL, &size, NULL, 0);
        char *machine = malloc(size);
        sysctlbyname("hw.machine", machine, &size, NULL, 0);
        _modelVersion = [NSString stringWithUTF8String:machine];
        free(machine);
    });
    return _modelVersion;
}

+ (NSString *)localizedModelVersion
{
    static NSString *_localizedModelVersion = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _localizedModelVersion = [self TDlocalizedModelVersion];
    });
    return _localizedModelVersion;
}

+ (NSString *)TDlocalizedModelVersion
{
    NSString *platform = [self modelVersion];
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone4(GSM)";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"iPhone4(CDMA)";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone5(GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone5(CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone5c";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone5c";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone5s";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone5s";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone6Plus";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone6";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone6sPlus";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone6s";
    if ([platform isEqualToString:@"iPhone8,4"])    return @"iPhoneSE";
    if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone7";
    if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone7";
    if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone7Plus";
    if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone7Plus";
    if ([platform isEqualToString:@"iPhone10,1"])   return @"iPhone8";
    if ([platform isEqualToString:@"iPhone10,4"])   return @"iPhone8";
    if ([platform isEqualToString:@"iPhone10,2"])   return @"iPhone8Plus";
    if ([platform isEqualToString:@"iPhone10,5"])   return @"iPhone8Plus";
    if ([platform isEqualToString:@"iPhone10,3"])   return @"iPhoneX";
    if ([platform isEqualToString:@"iPhone10,6"])   return @"iPhoneX";
    if ([platform isEqualToString:@"iPhone11,2"])   return @"iPhoneXS";
    if ([platform isEqualToString:@"iPhone11,4"])   return @"iPhoneXSMax";
    if ([platform isEqualToString:@"iPhone11,6"])   return @"iPhoneXSMax";
    if ([platform isEqualToString:@"iPhone11,8"])   return @"iPhoneXR";
    if ([platform isEqualToString:@"iPhone12,1"])   return @"iPhone11";
    if ([platform isEqualToString:@"iPhone12,3"])   return @"iPhone11Pro";
    if ([platform isEqualToString:@"iPhone12,5"])   return @"iPhone11ProMax";
    if ([platform isEqualToString:@"iPhone12,8"])   return @"iPhoneSE2";
    if ([platform isEqualToString:@"iPhone13,1"])   return @"iPhone12mini";
    if ([platform isEqualToString:@"iPhone13,2"])   return @"iPhone12";
    if ([platform isEqualToString:@"iPhone13,3"])   return @"iPhone12Pro";
    if ([platform isEqualToString:@"iPhone13,4"])   return @"iPhone12ProMax";
    if ([platform isEqualToString:@"iPhone14,4"])   return @"iPhone13mini";
    if ([platform isEqualToString:@"iPhone14,5"])   return @"iPhone13";
    if ([platform isEqualToString:@"iPhone14,2"])   return @"iPhone13Pro";
    if ([platform isEqualToString:@"iPhone14,3"])   return @"iPhone13ProMax";
    
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPodTouch1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPodTouch2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPodTouch3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPodTouch4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPodTouch5G";
    if ([platform isEqualToString:@"iPod7,1"])      return @"iPodTouch6G";
    if ([platform isEqualToString:@"iPod9,1"])      return @"iPodTouch7G";
    
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad2(WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad2(GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad2(CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad2(WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPadMini(WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPadMini(GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPadMini(CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad3(WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad3(CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad3(GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad4(WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad4(GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad4(CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPadAir(WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPadAir(GSM)";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPadAir(CDMA)";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPadMiniRetina(WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPadMiniRetina(Cellular)";
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPadMini3(WiFi)";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPadMini3(Cellular)";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPadMini3(Cellular)";
    if ([platform isEqualToString:@"iPad5,1"])      return @"iPadMini4(WiFi)";
    if ([platform isEqualToString:@"iPad5,2"])      return @"iPadMini4(Cellular)";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPadAir2(WiFi)";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPadAir2(Cellular)";
    if ([platform isEqualToString:@"iPad6,7"])      return @"iPadPro(WiFi)";
    if ([platform isEqualToString:@"iPad6,8"])      return @"iPadPro(Cellular)";
    
    if ([platform isEqualToString:@"i386"])         return [UIDevice currentDevice].model;
    if ([platform isEqualToString:@"x86_64"])       return [UIDevice currentDevice].model;
    
    return platform;
}

+ (UIEdgeInsets)safeAreaInset
{
    if ([UIApplication sharedApplication].keyWindow) {
        return [UIApplication sharedApplication].keyWindow.safeAreaInsets;
    }
    return UIEdgeInsetsZero;
}

@end
