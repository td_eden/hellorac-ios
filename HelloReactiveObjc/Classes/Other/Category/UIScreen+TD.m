//
//  UIScreen+TD.m
//  UIScreen
//
//  Created by 李同德 on 2020/7/1.
//  Copyright © 2020 李同德. All rights reserved.
//

#import "UIScreen+TD.h"
#import "UIDevice+TD.h"

@implementation UIScreen (TD)

+ (CGFloat)width {
    return UIDevice.screenWidth;
}

+ (CGFloat)height {
    return UIDevice.screenHeight;
}

@end
