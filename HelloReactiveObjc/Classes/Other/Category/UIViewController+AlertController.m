//
//  UIViewController+AlertController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/11/18.
//

#import "UIViewController+AlertController.h"

@interface TDAlertAction : UIAlertAction
@property (nonatomic, assign) NSInteger index;
@end

@implementation TDAlertAction
+ (instancetype)actionWithTitle:(nullable NSString *)title style:(UIAlertActionStyle)style index:(NSInteger)aIndex handler:(void (^ __nullable)(UIAlertAction *action, NSInteger index))handler {
    __block TDAlertAction *tdAlertAction = (TDAlertAction *)[super actionWithTitle:title style:style handler:^(UIAlertAction * _Nonnull action) {
        if (handler) {
            handler(action, tdAlertAction.index);
        }
    }];
    tdAlertAction.index = aIndex;
    return tdAlertAction;
}
@end


@implementation UIViewController (AlertController)

- (void)showAlertControllerWithTitle:(NSString *)title message:(NSString *)message actionTitles:(NSArray<NSString *> *)titles cancelTitle:(NSString *)cancelTitle preferredStyle:(UIAlertControllerStyle)preferredStyle callBack:(void (^)(NSInteger))callBack
{
    // 实例化UIAlertController
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:preferredStyle];
    
    // 处理 UIAlertAction（UIAlertActionStyleDefault）
    for (int index = 0; index < titles.count; index++) {
        TDAlertAction *defaultAction = [TDAlertAction actionWithTitle:titles[index] style:UIAlertActionStyleDefault index:index handler:^(UIAlertAction *action, NSInteger index) {
            if (callBack) {
                callBack(index);
            }
        }];
        [alertController addAction:defaultAction];
    }
    
    // 处理 UIAlertAction（UIAlertActionStyleCancel）
    if (cancelTitle.length > 0) {
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:cancelAction];
    }
    
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
