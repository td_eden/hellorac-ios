//
//  UIDevice+TD.h
//  UIDevice
//
//  Created by 李同德 on 2020/7/1.
//  Copyright © 2020 李同德. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIDevice (TD)

@property (nonatomic, class, readonly) CGFloat navigationHeight;

@property (nonatomic, class, readonly) CGFloat navigationBarHeight;
@property (nonatomic, class, readonly) CGFloat statusBarHeight;

@property (nonatomic, class, readonly) CGFloat screenWidth;
@property (nonatomic, class, readonly) CGFloat screenHeight;

@property (nonatomic, class, readonly) CGFloat tabBarHeight;

/// 判断是否是iPhoneX系列的手机
+ (BOOL)isIPhoneXSeries;

@end



/// 固件信息
@interface UIDevice (TDHardware)

+ (NSString *)modelVersion;

+ (NSString *)localizedModelVersion;

+ (UIEdgeInsets)safeAreaInset;

@end

NS_ASSUME_NONNULL_END
