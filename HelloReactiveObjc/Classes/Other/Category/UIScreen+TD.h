//
//  UIScreen+TD.h
//  UIScreen
//
//  Created by 李同德 on 2020/7/1.
//  Copyright © 2020 李同德. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIScreen (TD)

@property (class, nonatomic, readonly) CGFloat width;
@property (class, nonatomic, readonly) CGFloat height;

@end

NS_ASSUME_NONNULL_END
