#import <UIKit/UIKit.h>

#pragma mark - 基本使用

UIKIT_EXTERN NSString  * const TDRACTextField;
UIKIT_EXTERN NSString  * const TDRACButton;
UIKIT_EXTERN NSString  * const TDRACTime;
UIKIT_EXTERN NSString  * const TDRACProperty;
UIKIT_EXTERN NSString  * const TDRACIterate;
UIKIT_EXTERN NSString  * const TDRACNotification;
UIKIT_EXTERN NSString  * const TDRACDelegate;


#pragma mark - RAC常用类

UIKIT_EXTERN NSString  * const TDRACSignal;
UIKIT_EXTERN NSString  * const TDRACSubject;
UIKIT_EXTERN NSString  * const TDRACTuple;
UIKIT_EXTERN NSString  * const TDRACMulticastConnection;
UIKIT_EXTERN NSString  * const TDRACCommand;


#pragma mark - 高级函数使用

UIKIT_EXTERN NSString  * const TDRACCombination;
UIKIT_EXTERN NSString  * const TDRACMapping;
UIKIT_EXTERN NSString  * const TDRACFiltration;


#pragma mark - RAC+MVVM（项目实战）

UIKIT_EXTERN NSString  * const TDRACMVVMDemo;



#pragma mark - RAC-组合

UIKIT_EXTERN NSString  * const TDRACConcat;
UIKIT_EXTERN NSString  * const TDRACThen;
UIKIT_EXTERN NSString  * const TDRACMerge;
UIKIT_EXTERN NSString  * const TDRACZipWith;
UIKIT_EXTERN NSString  * const TDRACCombineLatest;
UIKIT_EXTERN NSString  * const TDRACReduce;


#pragma mark - RAC-映射

UIKIT_EXTERN NSString  * const TDRACMap;
UIKIT_EXTERN NSString  * const TDRACFlattenMap;


#pragma mark - RAC-过滤

UIKIT_EXTERN NSString  * const TDRACFilter;
UIKIT_EXTERN NSString  * const TDRACIgnore;
UIKIT_EXTERN NSString  * const TDRACDistinctUntilChanged;
UIKIT_EXTERN NSString  * const TDRACTake;
UIKIT_EXTERN NSString  * const TDRACSkip;
