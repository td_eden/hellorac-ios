//
//  TDRACBaseViewController.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/9.
//

#import "TDRACBaseViewController.h"

@interface TDRACBaseViewController ()

@end

@implementation TDRACBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
}

@end
