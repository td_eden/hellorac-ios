//
//  AppDelegate.h
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/5.
//

#import <UIKit/UIKit.h>

/**
 在RAC(RactiveCocoa)中，万物皆信号。
 */
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@end

