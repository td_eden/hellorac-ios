//
//  AppDelegate.m
//  HelloReactiveObjc
//
//  Created by 李同德 on 2021/8/5.
//

#import "AppDelegate.h"
#import "TDRACAPIViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:[TDRACAPIViewController new]];
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
