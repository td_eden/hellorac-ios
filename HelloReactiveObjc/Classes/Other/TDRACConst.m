#import <UIKit/UIKit.h>

#pragma mark - 基本使用

NSString  * const TDRACTextField = @"UITextField";
NSString  * const TDRACButton = @"UIButton";
NSString  * const TDRACTime = @"计时器（interval、delay）";
NSString  * const TDRACProperty = @"监听属性变化";
NSString  * const TDRACIterate = @"遍历数组和字典";
NSString  * const TDRACNotification = @"监听 Notification 通知事件";
NSString  * const TDRACDelegate = @"代替 Delegate 代理";


#pragma mark - RAC常用类

NSString  * const TDRACSignal = @"RACSignal（信号类）";
NSString  * const TDRACSubject = @"RACSubject（通常代替代理）";
NSString  * const TDRACTuple = @"RACTuple（元组类）";
NSString  * const TDRACMulticastConnection = @"RACMulticastConnection";
NSString  * const TDRACCommand = @"RACCommand（处理事件）";


#pragma mark - 高级函数使用

NSString  * const TDRACCombination = @"RAC-组合";
NSString  * const TDRACMapping = @"RAC-映射";
NSString  * const TDRACFiltration = @"RAC-过滤";


#pragma mark - RAC+MVVM（项目实战）

NSString  * const TDRACMVVMDemo = @"RAC+MVVV-DEMO";



#pragma mark - RAC-组合

NSString  * const TDRACConcat = @"concat";
NSString  * const TDRACThen = @"then";
NSString  * const TDRACMerge = @"merge";
NSString  * const TDRACZipWith = @"zipWith";
NSString  * const TDRACCombineLatest = @"combineLatest";
NSString  * const TDRACReduce = @"reduce";


#pragma mark - RAC-映射

NSString  * const TDRACMap = @"map";
NSString  * const TDRACFlattenMap = @"flattenMap";


#pragma mark - RAC-过滤

NSString  * const TDRACFilter = @"filter";
NSString  * const TDRACIgnore = @"ignore";
NSString  * const TDRACDistinctUntilChanged = @"distinctUntilChanged";
NSString  * const TDRACTake = @"take";
NSString  * const TDRACSkip = @"skip";
